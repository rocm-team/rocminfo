Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: AMD
Source: https://github.com/ROCm/rocminfo

Files: *
Copyright: 2014-2023, Advanced Micro Devices, Inc.
License: Illinois-NCSA
 The University of Illinois/NCSA
 Open Source License (NCSA)
 .
 Copyright (c) 2014-2017, Advanced Micro Devices, Inc. All rights reserved.
 .
 Developed by:
 .
                 AMD Research and AMD HSA Software Development
 .
                 Advanced Micro Devices, Inc.
 .
                 www.amd.com
 .
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to
 deal with the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
  - Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimers.
  - Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimers in
    the documentation and/or other materials provided with the distribution.
  - Neither the names of Advanced Micro Devices, Inc,
    nor the names of its contributors may be used to endorse or promote
    products derived from this Software without specific prior written
    permission.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS WITH THE SOFTWARE.

Files: debian/*
Copyright: 2019-2022, Mo Zhou <lumin@debian.org>
           2020, Norbert Preining <norbert@preining.info>
           2022-2023, Étienne Mollier <emollier@debian.org>
           2023,2024, Cordell Bloor <cgmb@slerp.xyz>,
           2024, Xuanteng Huang <xuanteng.huang@outlook.com>,
License: Expat

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

